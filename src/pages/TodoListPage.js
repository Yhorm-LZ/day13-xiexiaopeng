import TodoList from "../features/todo/components/todo/TodoList";

export default function TodoListPage() {
    return (
        <div className='todo-list'>
            <TodoList />
        </div>
    );
}