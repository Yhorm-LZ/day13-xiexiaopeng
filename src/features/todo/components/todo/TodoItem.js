import {useTodos} from "../../../hooks/useTodo";
import {CloseOutlined, EditOutlined} from "@ant-design/icons";
import { Modal } from 'antd';
import {useState} from "react";
import TextArea from "antd/es/input/TextArea";

export default function TodoItem({task}) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [newTaskName, setNewTaskName] = useState("");
    const {updateTodoStatus, deleteTodo,updateTaskName} = useTodos()
    const handleTaskNameClick = async () => {
        await updateTodoStatus(task)
    }

    const handleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(task)
        }
    }

    const handleNewTaskNameChange = async (e) => {
        const value = e.target.value;
        setNewTaskName(value);
    }

    const handleUpdateButtonClick = async () => {
        setIsModalOpen(true);
    }

    const handleOk=async ()=>{
        await updateTaskName(task,newTaskName)
        setIsModalOpen(false);
        setNewTaskName('')
    }

    const handleCancel=async ()=>{
        setIsModalOpen(false);
        setNewTaskName('')
    }

    return (
        <div className='todo-item'>
            <div className={`task-name ${task.done ? 'done' : ''}`} onClick={handleTaskNameClick}>
                {task.name}
            </div>
            <div className='buttons'>
                <div>
                    <EditOutlined onClick={handleUpdateButtonClick}/>
                </div>
                <div className='remove-button' onClick={handleRemoveButtonClick}>
                    <CloseOutlined/>
                </div>
            </div>
            <Modal title="Basic Modal" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <TextArea value={newTaskName} onChange={handleNewTaskNameChange} rows={4} placeholder="Please input the content."/>
            </Modal>
        </div>
    );
}