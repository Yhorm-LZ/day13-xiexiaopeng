import './App.css';
import {Outlet} from "react-router-dom";
import NavBar from "./features/todo/components/done/NavBar";

function App() {
    return (
        <div className="app">
            <NavBar/>
            <Outlet></Outlet>
        </div>
    );
}

export default App;
