import api from "./api";

export const getTodoTask = () => {
    return api.get('/todoItem')
}

export const updateTodoTask = (id, todoItem) => {
    return api.put(`/todoItem/${id}`, todoItem)
}

export const deleteTodoTask = (id) => {
    return api.delete(`/todoItem/${id}`)
}

export const addTodoTask = (todoItem) => {
    return api.post(`/todoItem/`, todoItem)
}