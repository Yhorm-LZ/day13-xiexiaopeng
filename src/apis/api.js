import axios from "axios";

const api = axios.create(
    {
        baseURL: 'https://64c0b67a0d8e251fd112656a.mockapi.io/todoList',
    });

export default api;